/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Hibernate;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author student
 */
public class FilesQuery {

    private Session session = null;
    private Query q = null;
    private ArrayList<Files> fileList = null;
    
    public int getListLength(){
        return fileList.size();
    }
    
    public List<Files> createFileList() {
        try {
            org.hibernate.Transaction tx = session.beginTransaction();
            q = session.createQuery("from Files");
            fileList = (ArrayList<Files>) (List<Files>) q.list();
            session.close();
            tx.commit();
        } catch (HibernateException e) {
        }
        return fileList;
    }

    public ArrayList<Files> getFileList() {
        return fileList;
    }

    public void uploadHibernate(String nazwa, String opis) {
        if (nazwa.endsWith(".xls")){
        if (searchName(createFileList(), nazwa)) {
            updateFile(nazwa, opis);
        } else {
            addFile(nazwa, opis);
        }
        }
    }

    public void addFile(String nazwa, String opis) {

        Files file = new Files(nazwa, opis);
        try {
            Session session = NewHibernateUtil.getSessionFactory().getCurrentSession();
            Transaction tx = session.beginTransaction();
            session.save(file);
            tx.commit();
            session.close();

        } catch (HibernateException e) {
        }
    }

    public void updateFile(String name, String opis) {
        Session session = NewHibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = session.beginTransaction();
        q = session.createQuery("update Files set opis ='" + opis + "' where  nazwa = '" + name + "'");
        q.executeUpdate();

        session.close();
        tx.commit();
    }

    public void deleteFile(String name) {
        Session session = NewHibernateUtil.getSessionFactory().getCurrentSession();

        Transaction tx = session.beginTransaction();
        q = session.createQuery("delete Files where nazwa = '" + name+"'");
        q.executeUpdate();

        session.close();
        tx.commit();
    }

    public boolean searchName(List<Files> list, String name) {
        for (Files file : list) {
            if (file.getNazwa().equals(name)) {
                return true;
            }
        }
        return false;
    }
    public Files getFiles(List<Files> list, String name) {
        for (Files file : list) {
            if (file.getNazwa().equals(name)) {
                return file;
            }
        }
        return null;
    }
    public FilesQuery() {
        this.session = NewHibernateUtil.getSessionFactory().getCurrentSession();
    }

}
