/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Hibernate.FilesQuery;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author student
 */
@WebServlet(name = "Opis", urlPatterns = {"/Opis"})
public class Opis extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String nazwa = request.getParameter("var");
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Opis</title>");
            out.println("<link rel=\"stylesheet\" href=\"style.css\">");

            out.println("</head>");
            out.println("<body>");
            out.println("<h3>Nazwa pliku: </h3><p style=\"font-size: 15px;color: white;margin-left: 40px;\">" + nazwa + "</p>");
            out.println("<br><h3>Opis: </h3><p style=\"font-size: 15px;color: white;margin-left: 40px;\">" + new FilesQuery().getFiles(new FilesQuery().createFileList(), nazwa).getOpis() + "</p>");
            out.println("<form action=\"./Download\" > "
                    + "<input style=\"float:left;\"  type=\"hidden\" name=\"var\" value=\"" + nazwa + "\" />"
                    + "<input class=\"download\" type=\"submit\" value=\"Pobierz\">"
                    + "</form>");
            out.println("<form action=\"./Delete\" >"
                    + "<input style=\"float:left;\" type=\"hidden\" name=\"var\" value=\"" + nazwa + "\" />"
                    + "<input class=\"delete\" type=\"submit\" value=\"Usun\">"
                    + "</form>");
            
            out.println("<form action=\"./\"><input class=\"powrot\" type=\"submit\" value=\"Powrot\">\n" +
"            </form> ");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
