/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Hibernate.FilesQuery;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import javax.xml.ws.WebServiceRef;
import m_soap.ClassNotFoundException_Exception;
import m_soap.IOException_Exception;
import m_soap.NewWebService_Service;

/**
 *
 * @author student
 */
@MultipartConfig
@WebServlet(name = "Upload", urlPatterns = {"/Upload"})
public class Upload extends HttpServlet {

    @WebServiceRef(wsdlLocation = "http://localhost:8080/NewWebService/NewWebService?WSDL")
    private NewWebService_Service service;
    private String opis;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Upload</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        Part filePart = request.getPart("fileToUpload");
        opis=request.getParameter("Opis");
        BufferedInputStream inputStream = new BufferedInputStream(filePart.getInputStream());
        byte[] imageBytes = new byte[(int) filePart.getSize()];
        inputStream.read(imageBytes);

        try {
            upload(filePart.getSubmittedFileName(), imageBytes);
        } catch (ClassNotFoundException_Exception | IOException_Exception ex) {
            Logger.getLogger(Upload.class.getName()).log(Level.SEVERE, null, ex);
        }
        inputStream.close();


        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Upload</title>");
            out.println("<link rel=\"stylesheet\" href=\"style.css\">");

            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Plik</h1>");
            if (filePart.getSubmittedFileName().endsWith(".xls")){
            out.print("<h2>Nazwa: " + filePart.getSubmittedFileName() + "</h2>");
            out.print("<h2>Opis: " + opis + "</h2>");
            }else{
            out.print("<h2>Zły plik</h2><h2>Podaj plik z końcówką .xls</h2>");
            }
            out.println("<form action=\"./\"><input class=\"powrot\" type=\"submit\" value=\"Powrot\">\n" +
"            </form> ");
            out.println("</body>");
            out.println("</html>");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void upload(java.lang.String nazwa, byte[] imageBytes) throws ClassNotFoundException_Exception, IOException_Exception {
        // Note that the injected javax.xml.ws.Service reference as well as port objects are not thread safe.
        // If the calling of port operations may lead to race condition some synchronization is required.
        m_soap.NewWebService port = service.getNewWebServicePort();
                new FilesQuery().uploadHibernate(nazwa, opis);
        port.upload(nazwa, imageBytes);
    }

}
