/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package m_soap;

import Beans.Kontroler;
import java.io.IOException;
import javax.ejb.EJB;
import javax.jws.WebService;
import javax.ejb.Stateless;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author student
 */
@WebService(serviceName = "NewWebService")
@Stateless()
public class NewWebService {

    @EJB
    private Kontroler ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "getMessage")
    public String getMessage() {
        return ejbRef.getMessage();
    }

    @WebMethod(operationName = "setMessage")
    @Oneway
    public void setMessage(@WebParam(name = "message") String message) {
        ejbRef.setMessage(message);
    }

    @WebMethod(operationName = "delete")
    @Oneway
    public void delete(@WebParam(name = "nazwa") String nazwa) {
        ejbRef.delete(nazwa);
    }

    @WebMethod(operationName = "download")
    public byte[] download(@WebParam(name = "nazwa") String nazwa) throws IOException {
        return ejbRef.download(nazwa);
    }

    @WebMethod(operationName = "upload")
    public void upload(@WebParam(name = "nazwa") String nazwa, @WebParam(name = "imageBytes") byte[] imageBytes) throws IOException, ClassNotFoundException {
        ejbRef.upload(nazwa, imageBytes);
    }
    
}
