package Beans;

import javax.ejb.Stateless;
import javax.ejb.LocalBean;

/**
 *
 * @author student
 */
@Stateless
@LocalBean
public class UploadBean {

    String uploadMessage;
    String opis;
    String nazwa;
    static public final String folderPath = "/home/student/Desktop/TJEE/projekt/WCY18II1S1_Kurenda_Server/uploaded-files/";
    
    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }
   
    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public String getUploadMessage() {
        return uploadMessage;
    }

    public void setUploadMessage(String uploadMessage) {
        this.uploadMessage = uploadMessage;
    }

}
