/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Beans;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;


/**
 *
 * @author student
 */
@Stateless
@LocalBean
public class Kontroler {
    String message;
    

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    
    
    public void delete(String nazwa){
          File f = new File( UploadBean.folderPath + nazwa);
         if(f.delete())
         {
             setMessage("Usunieto");
         }
         else
         {
             setMessage("Nie usunieto");
         }

    }
    public byte[] download(String nazwa) throws IOException  {
        String path = UploadBean.folderPath + nazwa;
        File file = new File(path);
            FileInputStream fileInputStream = new FileInputStream(UploadBean.folderPath + nazwa);
            BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
            byte[] fileBytes = new byte[(int) file.length()];
            bufferedInputStream.read(fileBytes);
            bufferedInputStream.close();
             
            return fileBytes;
    }
    public void upload(String nazwa, byte[] imageBytes) throws IOException, ClassNotFoundException {
        if (nazwa.endsWith(".xls")) {
            
            String path = UploadBean.folderPath + nazwa;  
            
            FileOutputStream fos = new FileOutputStream(path);
            BufferedOutputStream outputStream = new BufferedOutputStream(fos);
            outputStream.write(imageBytes);
            outputStream.close();
             
                   
        }
    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    }
}
